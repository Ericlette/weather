**Eric Letterucci - Eriiclette@gmail.com**

# Microservicio de Clima

Este proyecto presenta un microservicio desarrollado en Spring Boot (versión 2.7.8) con Java 8, diseñado para proporcionar información actualizada sobre las condiciones climáticas de las ciudades más importantes en diferentes continentes. A continuación, se detallan los principales puntos de acceso y su funcionalidad.

## Endpoints

### Obtener Condiciones Climáticas Actuales por Ciudad
- **URL:** http://localhost:8080/weather/current/{locationKey}
- **{locationKey}:** Código de la ciudad para la cual se desea obtener la información climática.

Este endpoint devuelve la información climática actual para la ciudad especificada. Si la información está en la base de datos y la hora de ingreso no supera los 10 minutos, se retorna la información almacenada. En caso contrario, se consulta la API del clima, se actualiza la información en la base de datos y luego se devuelve la información.

### Obtener Lista de las 150 Principales Ciudades del Mundo
- **URL:** http://localhost:8080/weather/cities

Este endpoint retorna una lista de las principales ciudades más importantes del mundo. Al igual que en el endpoint anterior, primero se realiza una verificación para determinar si la información está en la base de datos. En caso afirmativo, se retorna una lista completa; de lo contrario, se consulta la API de AccuWeather para obtener información actualizada, almacenarla en la base de datos y posteriormente devolverla.

## Uso del Servicio

Para interactuar con el microservicio, simplemente accede a los endpoints mencionados utilizando las URLs proporcionadas y ajustando los parámetros según tus necesidades.

**Importante:** Asegúrate de tener configurado y activo tu entorno local de desarrollo. Ten en cuenta que la información climática se actualiza automáticamente si han pasado más de 10 minutos desde la última actualización.

## Tecnologías Utilizadas

- Java 8
- Spring Boot 2.7.8
- Base de Datos (se utiliza para almacenar información climática)
- AccuWeather API (https://developer.accuweather.com)

## Configuración de entorno

Este proyecto para gestionar de forma segura las APIKEY y las URLs necesarias para obtener información del clima desde la API de AccuWeather, toma el valor de las variables desde el entorno, por lo tanto, asegúrate de configurar adecuadamente las siguientes variables de entorno en tu sistema para alimentar las propiedades del archivo `application.properties`:

```bash
# Variables de entorno necesarias
export ACCUWEATHER_API_KEY=4VGzbHAxpULLGIp6bLTDnjCBKszIK3Gh 
export ACCUWEATHER_API_BASE_URL=http://dataservice.accuweather.com
export ACCUWEATHER_API_TOP_CITIES_URL=/currentconditions/v1/topcities/
export ACCUWEATHER_API_CURRENT_CONDITIONS_URL=/currentconditions/v1/
```
Recuerda que el APIKEY expira luego de 50 peticiones. Si deseas modificarlo u obtener uno propio, dirigite a la siguiente URL: https://developer.accuweather.com 


## Comandos Maven

Para ejecutar el proyecto después de clonar el repositorio, sigue estos pasos:

1. Abre una terminal en el directorio raíz del proyecto.

2. Ejecuta el siguiente comando para compilar el proyecto:


```bash
mvn clean install 
```

3. Ejecuta el proyecto 

```bash
mvn spring-boot:run
```

Estos comandos aseguran que Maven descargue las dependencias, compile el proyecto y ejecute la aplicación Spring Boot. Después de ejecutar estos comandos, podrás acceder a los endpoints mencionados anteriormente. Asegúrate de tener un entorno de desarrollo Java y Maven correctamente configurado en tu sistema. ¡Disfruta explorando el microservicio de clima!
