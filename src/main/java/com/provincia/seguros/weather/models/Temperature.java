package com.provincia.seguros.weather.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Temperature {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(cascade = CascadeType.ALL)
    @JsonProperty("Metric")
    @JoinColumn(name = "metric_id")
    private Metric metric;

    @OneToOne(cascade = CascadeType.ALL)
    @JsonProperty("Imperial")
    @JoinColumn(name = "imperial_id")
    private Imperial imperial;
}