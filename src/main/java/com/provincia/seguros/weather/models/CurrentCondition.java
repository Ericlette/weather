package com.provincia.seguros.weather.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;


@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "current_condition")
public class CurrentCondition {


    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Id
    @Column(name = "location_key")
    private long locationKey;

    @JsonProperty("LocalObservationDateTime")
    @Column(name = "local_observation_datetime")
    private String localObservationDateTime;

    @JsonProperty("EpochTime")
    @Column(name = "epoch_time")
    private Long epochTime;

    @JsonProperty("WeatherText")
    @Column(name = "weather_text")
    private String weatherText;

    @JsonProperty("WeatherIcon")
    @Column(name = "weather_icon")
    private Integer weatherIcon;

    @JsonProperty("HasPrecipitation")
    @Column(name = "has_precipitation")
    private Boolean hasPrecipitation;

    @JsonProperty("PrecipitationType")
    @Column(name = "precipitation_type")
    private String precipitationType;

    @JsonProperty("IsDayTime")
    @Column(name = "is_daytime")
    private Boolean isDayTime;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "temperature_id")
    @JsonProperty("Temperature")
    private Temperature temperature;

    @JsonProperty("MobileLink")
    @Column(name = "mobile_link")
    private String mobileLink;

    @JsonProperty("Link")
    @Column(name = "link")
    private String link;
}
