package com.provincia.seguros.weather.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "top_cities")
public class TopCities {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "location_key")
    @JsonProperty("key")
    private long locationKey;

    @JsonProperty("LocalizedName")
    @Column(name = "localized_name")
    private String localizedName;

}
