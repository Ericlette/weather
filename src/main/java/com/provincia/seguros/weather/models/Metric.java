package com.provincia.seguros.weather.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Metric {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonProperty("Value")
    @Column(name = "value_metric")
    private double value;

    @JsonProperty("Unit")
    @Column(name = "unit")
    private String unit;

    @JsonProperty("UnitType")
    @Column(name = "unit_type")
    private int unitType;

}