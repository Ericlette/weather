package com.provincia.seguros.weather.repository;

import com.provincia.seguros.weather.models.CurrentCondition;
import com.provincia.seguros.weather.models.TopCities;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TopCitiesRepository extends JpaRepository<TopCities, Long> {
}