package com.provincia.seguros.weather.repository;

import com.provincia.seguros.weather.models.CurrentCondition;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CurrentConditionRepository extends JpaRepository<CurrentCondition, Long> {
}