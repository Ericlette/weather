package com.provincia.seguros.weather.exceptions;

public class AccuWeatherApiException extends RuntimeException {

    public AccuWeatherApiException(String message, Throwable cause) {
        super(message, cause);
    }
    public AccuWeatherApiException(String message) {
        super(message);
    }
}