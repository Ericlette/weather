package com.provincia.seguros.weather.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CurrentConditionDTO {

    private String Country;
    private String dateTime;
    private String description;
    private double temperatureInFahrenheit;
    private double temperatureInCelsius;
    private String Link;

}
