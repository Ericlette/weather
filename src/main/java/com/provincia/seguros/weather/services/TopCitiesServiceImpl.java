package com.provincia.seguros.weather.services;

import com.provincia.seguros.weather.models.TopCities;
import com.provincia.seguros.weather.repository.TopCitiesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class TopCitiesServiceImpl implements TopCitiesService {

    @Autowired
    private TopCitiesRepository repository;

    @Override
    public List<TopCities> getAllTopCities() {
        return repository.findAll();
    }

    @Override
    public void saveAll(Iterable<TopCities> cities) {
         repository.saveAll(cities);
    }
}
