package com.provincia.seguros.weather.services;

import com.provincia.seguros.weather.exceptions.AccuWeatherApiException;
import com.provincia.seguros.weather.models.CurrentCondition;
import com.provincia.seguros.weather.models.TopCities;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import java.util.Collections;
import java.util.List;

@Service
public class AccuWeatherApiService {

    private final RestTemplate restTemplate;

    @Value("${accuweather.api.url.base}")
    private String baseUrl;

    @Value("${accuweather.api.url.current-conditions}")
    private String currentConditionsUrl;

    @Value("${accuweather.api.url.top-cities}")
    private String topCitiesUrl;

    @Value("${accuweather.api.api-key}")
    private String apiKey;

    public AccuWeatherApiService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    /** 
     * Metodo que extrae desde la API AccuWeather la informacion necesaria del clima 
     * de una ciudad en particular, mapea la información con los modelos correspondientes.
     * @param locationKey
     * @return CurrentCondition
     */
    public CurrentCondition getCurrentConditions(String locationKey) {
        try {

            ResponseEntity<CurrentCondition[]> responseEntity = restTemplate.getForEntity(buildUrl(currentConditionsUrl, locationKey), CurrentCondition[].class);
            if (responseEntity.getStatusCode().is2xxSuccessful() && responseEntity.getBody() != null && responseEntity.getBody().length > 0)
                return responseEntity.getBody()[0];
            else
                throw new AccuWeatherApiException("Error al llamar a la API de AccuWeather");

        } catch (HttpClientErrorException ex) {
            throw new AccuWeatherApiException("Error al llamar a la API de AccuWeather", ex);
        }
    }
    
    /** 
     * Metodo que extrae desde la API AccuWeather la información de  
     * las ciudades mas importantes disponibles. 
     * Mapea la informacion con los modelos correspondientes. 
     * @return List<TopCities>
     */
    public List<TopCities> getTopCities() {

        try {
            ResponseEntity<List<TopCities>> responseEntity = restTemplate.exchange(
                    buildUrl(topCitiesUrl, "150"),
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<List<TopCities>>() {});

            if (responseEntity.getStatusCode().is2xxSuccessful()) {
                List<TopCities> topCities = responseEntity.getBody();
                if (topCities != null)
                    return topCities;

            } else
                throw new AccuWeatherApiException("Error al llamar a la API de AccuWeather");


        } catch (HttpClientErrorException ex) {
            throw new AccuWeatherApiException("Error al llamar a la API de AccuWeather", ex);
        }

        return Collections.emptyList();

    }
    
    /** 
     * 
     * Metodo que construye la URL para comunicarnos con la API AccuWeather
     * @param apiUrl
     * @param uriVariables
     * @return String
     */
    private String buildUrl(String apiUrl, String uriVariables) {
        return UriComponentsBuilder
                .fromUriString(baseUrl)
                .path(apiUrl)
                .path(uriVariables)
                .queryParam("apikey", apiKey)
                .queryParam("details", "false")
                .toUriString();
    }
}
