package com.provincia.seguros.weather.services;

import com.provincia.seguros.weather.models.TopCities;

import java.util.List;

public interface TopCitiesService {

    List<TopCities> getAllTopCities();

    void saveAll(Iterable<TopCities> entities);

}
