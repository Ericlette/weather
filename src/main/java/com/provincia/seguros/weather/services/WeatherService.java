package com.provincia.seguros.weather.services;

import com.provincia.seguros.weather.dto.CurrentConditionDTO;
import com.provincia.seguros.weather.models.CurrentCondition;
import com.provincia.seguros.weather.models.TopCities;
import com.provincia.seguros.weather.repository.CurrentConditionRepository;
import com.provincia.seguros.weather.repository.TopCitiesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.List;


@Service
public class WeatherService {

    @Autowired
    private AccuWeatherApiService accuWeatherApiService;

    @Autowired
    private CurrentConditionService currentConditionService;

    @Autowired
    private CurrentConditionRepository currentConditionRepository;

    @Autowired
    private TopCitiesRepository TopCitiesRepository;

    @Autowired
    private TopCitiesService TopCitiesService;
    
    /** 
     * Metodo que retorna la informacion del clima de una ciudad en especifica. 
     * Determina si existe informacion en la base de datos y que la misma este actualizada. 
     * Caso contrario llama a metodo para busca informacion en la API.
     * @param locationKey
     * @return CurrentConditionDTO
     */
    public CurrentConditionDTO getCurrentConditions(String locationKey) {

        if (currentConditionRepository.existsById(Long.parseLong(locationKey))) {
            CurrentCondition currentCondition = currentConditionService.getCurrentConditionById(Long.parseLong(locationKey)).orElse(null);
            if (currentCondition != null && validateTimeDifference(currentCondition.getLocalObservationDateTime())) {
                return convertCurrentCondition(findStoreCurrentCondition(locationKey));
            } else {
                return convertCurrentCondition(currentCondition);
            }
        } else {
            return convertCurrentCondition(findStoreCurrentCondition(locationKey));
        }
    }

    /** 
     * Metodo que retorna la lista de ciudades disponibles.
     * Determina si existen ciudades en la base de datos o no.
     * En caso de no encontrar, llama a un metodo que busque desde la API.
     * @return List<TopCities>
     */
    public List<TopCities> getTopCities() {
        if (TopCitiesRepository.count() > 0)
            return TopCitiesService.getAllTopCities();
        else
            return findStoreTopCities();

    }

    /** 
     * Metodo que compara la fecha y hora actual, con la del registro en la BD. 
     * @param dateTimeBD
     * @return boolean
     */
    private boolean validateTimeDifference(String dateTimeBD) {
        OffsetDateTime dateTimeDB = OffsetDateTime.parse(dateTimeBD);
        OffsetDateTime horaActual = OffsetDateTime.now(ZoneOffset.ofHours(-3));
        return ChronoUnit.MINUTES.between(horaActual, dateTimeDB) > 10;
    }

    /** 
     * Metodo que busca la informacion del clima desde la API AccuWeather
     * Almacena en la base de datos.
     * @param locationKey
     * @return CurrentCondition
     */
    private CurrentCondition findStoreCurrentCondition(String locationKey) {

        CurrentCondition cc = accuWeatherApiService.getCurrentConditions(locationKey);
        cc.setLocationKey(Long.parseLong(locationKey));
        return currentConditionService.createCurrentCondition(cc);
    }
    
    /** 
     * Metodo que busca la lista de paises desde la API AccuWeather.
     * Almacena en la base de datos.
     * @return List<TopCities>
     */
    private List<TopCities> findStoreTopCities() {

        TopCitiesService.saveAll((Iterable<TopCities>) accuWeatherApiService.getTopCities());
        return TopCitiesService.getAllTopCities();
    }
    
    /** 
     * Metodo que parsea la informacion de la base de datos, 
     * para renderizar unicamente la informacion que necesitamos. 
     * @param cc
     * @return CurrentConditionDTO
     */
    private CurrentConditionDTO convertCurrentCondition(CurrentCondition cc) {

        CurrentConditionDTO ccDTO = new CurrentConditionDTO();

        ccDTO.setDateTime(cc.getLocalObservationDateTime());
        ccDTO.setDescription(cc.getWeatherText());
        ccDTO.setTemperatureInFahrenheit(cc.getTemperature().getImperial().getValue());
        ccDTO.setTemperatureInCelsius(cc.getTemperature().getMetric().getValue());
        ccDTO.setLink(cc.getLink());

        return ccDTO;

    }
}
