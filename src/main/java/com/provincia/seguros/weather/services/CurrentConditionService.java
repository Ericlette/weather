package com.provincia.seguros.weather.services;

import com.provincia.seguros.weather.models.CurrentCondition;

import java.util.Optional;

public interface CurrentConditionService {

    CurrentCondition createCurrentCondition(CurrentCondition currentCondition);

    Optional<CurrentCondition> getCurrentConditionById(Long id);

}
