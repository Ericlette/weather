package com.provincia.seguros.weather.services;

import com.provincia.seguros.weather.models.CurrentCondition;
import com.provincia.seguros.weather.repository.CurrentConditionRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CurrentConditionServiceImpl implements CurrentConditionService {

    private final CurrentConditionRepository repository;

    public CurrentConditionServiceImpl(CurrentConditionRepository repository) {
        this.repository = repository;
    }

    @Override
    public CurrentCondition createCurrentCondition(CurrentCondition currentCondition) {
        return repository.save(currentCondition);
    }

    @Override
    public Optional<CurrentCondition> getCurrentConditionById(Long id) {
        return repository.findById(id);
    }


}
