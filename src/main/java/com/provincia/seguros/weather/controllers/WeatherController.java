package com.provincia.seguros.weather.controllers;

import com.provincia.seguros.weather.dto.CurrentConditionDTO;
import com.provincia.seguros.weather.services.WeatherService;
import com.provincia.seguros.weather.util.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.validation.constraints.NotBlank;

@RestController
@RequestMapping("/weather")
@Validated
public class WeatherController {
    @Autowired
    private WeatherService weatherService;

    private static final Logger logger = LoggerFactory.getLogger(WeatherController.class);

    /**
     * Maneja petición HTTP para el endpoint de la información del clima.
     * @param locationKey
     * @return Json
     *         
     */
    @GetMapping("/current/{locationKey}")
    public ResponseEntity<Object> getCurrentConditions(
            @PathVariable @NotBlank(message = "locationKey no puede estar en blanco") String locationKey) {
        try {
            CurrentConditionDTO response = weatherService.getCurrentConditions(locationKey);
            return ResponseEntity.ok().body(new ApiResponse("success", response));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new ApiResponse("error", "Error al procesar la solicitud: " + ex.getMessage()));
        }
    }

    /**
     * Maneja petición HTTP para el endpoint  lista de ciudades mas importantes disponibles.
     * 
     * @return Json List Cities
     *         
     */
    @GetMapping("/cities")
    public ResponseEntity<Object> getCountriesForTopCities() {
        try {
            ApiResponse apiResponse = new ApiResponse("success", weatherService.getTopCities());
            return ResponseEntity.ok().body(apiResponse);
        } catch (Exception ex) {
            logger.error("Error al procesar la solicitud", ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new ApiResponse("error", "Error al procesar la solicitud: " + ex.getMessage()));
        }
    }

}
