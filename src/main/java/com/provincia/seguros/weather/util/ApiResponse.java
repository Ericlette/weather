package com.provincia.seguros.weather.util;

public class ApiResponse {
    private final String status;
    private final Object data;

    public ApiResponse(String status, Object data) {
        this.status = status;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public Object getData() {
        return data;
    }
}